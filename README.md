# Demo Project - Deploy Microservices application in Kubernetes with Production & Security Best Practices

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Best Practices](#best-practices)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create K8s manifests for Deployments and Services for all microservices of an online shop application 

* Deploy microservices to Linode’s managed Kubernetes cluster

## Technologies Used 

* Kubernetes 

* Redis 

* Linux 

* Linode LKE

## Steps 

Step 1: Create config file which will contain deployments and services for every single micrservices 

Note: Set needed environmental variables for microservices containers that need to communicate with eachother  

Step 2: When finished with config file Log into linode on the browser 

[Linode](/images/01_go_on_linode.png)

Step 3: Start a kubernetes cluster with three worker nodes on linode 
[Worker nodes setup](/images/02_start_kubernetes_cluster_with_3_worker_nodes.png)

Step 4: When the linode kubernetes cluster start download kube config file 

[Kube config file](/images/03_download_kubeconfig_file.png)

Step 5: Set stricter permissions for kube config file 

    chmod 400 online-shop-microservices-kubeconfig.yaml

[Strict Permissions](/images/04_setting_stricter_permissions_for_kubeconfigfile.png)

Step 6: Export KUBECONFIG to point to kubeconfigfile to login into the linode server to access the kubernetes cluster 

    export KUBECONFIG=/Downloads/online-shop-microservices-kubeconfig.yaml

[Linode server login](/images/05_export_kubeconfig_to_point_to_kubeconfigfile.png)

Step 7 Check if you are connected to Linode Kluster checking which worker nodes you have available 

    kubectl get node

[Connection to Linode cluster](/images/06_check_if_connected_to_linode_kluster.png)

Step 8: Create name space for microservices

    kubectl create ns microservices 

[Microservices Namespace](/images/07_create_name_space_for_micro_service_.png)

Step 9: Apply microservices 

    kubectl apply -f config.yaml -n microservices

[Applying Microservices](/images/08_apply_microservice.png)

Step 10: Check if all pods are running for the microservices

    kubectl get pod -n microservices 

[Microservices pods](/images/09_check_if_pods_are_running.png) 

Step 11: Test application on browser 

[Application on browser](/images/10_application_on_browser.png) 

## Best Practices

* Included image version 

* liveness prob - simply script that pings application endpoint every 5 seconds or 10 seconds

* Readiness prob - This is how k8 knows that an application is fully started and ready to recieve the requests. It lets k8 know that the application is ready to recieve traffic and this is important because if your application needs two minutes to start all the request to it will fail during those two minutes creating a lot of errors in your applications. 

* Set up Environmental Variables - setting environoment variables to tell application to disable profile and tracing services we dont have them in the cluster 

* Resource Request -  Some application may need more cpu and memory resources than others 

* Limit - To limit the amount of cpu and memory of a container to avoid one container using all the cpu and memory of a node 


## Installation

Run $ brew install minikube 

## Usage 

    kubectl apply -f config.yaml -n microservices

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/k8-deploy-microservices-application-in-kubernetes.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/k8-deploy-microservices-application-in-kubernetes

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.